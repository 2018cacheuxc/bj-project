from django.test import TestCase
from django.urls import reverse, resolve

from jerecherche.models import Student_propose, Student_recherche, Nature, Lending
from jerecherche.views import index, liste_jerecherche, description_jerecherche, search_jerecherche

class BookingPageTestCase(TestCase):

    def setUp(self):
        Student_recherche.objects.create(name="John", mail="john@gmail.com")
        lampe = Nature.objects.create(name="Lampe")
        rideau = Student_propose.objects.create(name="Rideau")
        lampe.students.add(rideau)
        self.product = Nature.objects.get(name='Lampe')
        self.liaison = Student_recherche.objects.get(name='John')

    # test that a new booking is made
    def test_new_booking_is_registered(self):
        old_bookings = Lending.objects.count()
        product_id = self.product.id
        name = self.liaison.name
        mail =  self.liaison.mail
        response = self.client.post(reverse('jerecherche:description_jerecherche', args=(product_id,)), {
            'name': name,
            'mail': mail
        })
        new_bookings = Lending.objects.count()
        self.assertEqual(new_bookings, old_bookings + 1)

    # test that a booking belongs to a contact
    def test_new_booking_belongs_to_a_student_qui_recherche_un_produit(self):
        product_id = self.product.id
        name = self.liaison.name
        mail =  self.liaison.mail
        response = self.client.post(reverse('jerecherche:description_jerecherche', args=(product_id,)), {
            'name': name,
            'mail': mail
        })
        booking = Lending.objects.first()
        self.assertEqual(self.liaison, booking.liaison)

    # teste qu'une réservation appartient à un produit
    def test_new_booking_belongs_to_a_product(self):
        product_id = self.product.id
        name = self.liaison.name
        mail =  self.liaison.mail
        response = self.client.post(reverse('jerecherche:description_jerecherche', args=(product_id,)), {
            'name': name,
            'mail': mail
        })
        booking = Lending.objects.first()
        self.assertEqual(self.product, booking.product)

    # teste qu'un produit n'est plus disponible après réservation
    def test_album_not_available_if_booked(self):
        product_id = self.product.id
        name = self.liaison.name
        mail =  self.liaison.mail
        response = self.client.post(reverse('jerecherche:description_jerecherche', args=(product_id,)), {
            'name': name,
            'mail': mail
        })
        # Réalise la demande une nouvelle fois, sinon `available` reste `True`
        self.product.refresh_from_db()
        self.assertFalse(self.product.available)
