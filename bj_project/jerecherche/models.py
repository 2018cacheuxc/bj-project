from django.db import models
import datetime
from django.utils import timezone


class Student_propose(models.Model):
    name = models.CharField(max_length=40)
    mail = models.EmailField()
    
    def __str__(self):
        return self.name

class Student_recherche(models.Model):
    name = models.CharField(max_length=40)
    mail = models.EmailField()

    def __str__(self):
        return self.name

class Nature(models.Model):
    name = models.CharField(max_length=40)
    available = models.BooleanField(default=True)
    picture = models.TextField(default=True)
    students = models.ManyToManyField(Student_propose, related_name='products', blank=True)
    
    def __str__(self):
        return self.name

class Lending(models.Model):
    duree = models.DateTimeField(auto_now_add=True)
    contacte = models.BooleanField(default=False)
    product = models.OneToOneField(Nature,on_delete=models.CASCADE)
    liaison = models.ForeignKey(Student_recherche, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.duree)

