from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import include, url 
from django.views.generic import ListView
import jerecherche.views
from jerecherche.models import Student_propose, Student_recherche, Nature, Lending

app_name='jerecherche'
urlpatterns = [    
    url(r'^$', jerecherche.views.liste_jerecherche, name='liste_jerecherche'),
    url(r'^(?P<product_id>[0-9]+)/$', jerecherche.views.description_jerecherche, name='description_jerecherche'),
    url(r'^search/$', jerecherche.views.search_jerecherche, name='search_jerecherche'),
]
