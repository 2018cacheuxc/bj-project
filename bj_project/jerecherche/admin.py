from django.contrib import admin
from django.utils.safestring import mark_safe
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse

#on importe les tables utiles pour pouvoir les modifier depuis l'interface administrateur
from jerecherche.models import Student_propose, Student_recherche, Nature, Lending

class AdminURLMixin(object):
    def get_admin_url(self, obj):
        content_type = ContentType.objects.get_for_model(obj.__class__)
        return reverse("admin:jerecherche_%s_change" % (
            content_type.model),
            args=(obj.id,))


@admin.register(Lending)
class LendingAdmin(admin.ModelAdmin, AdminURLMixin):
    list_filter = ['duree']
    fields = ["duree", "liaison_link", 'product_link']
    readonly_fields = ["duree", "liaison_link", "product_link"]

    def has_add_permission(self, request):
        return False

    def liaison_link(self, lending):
        url = self.get_admin_url(lending.liaison)
        return mark_safe("<a href='{}'>{}</a>".format(url, lending.laison.name))

    def product_link(self, lending):
        url = self.get_admin_url(lending.album)
        return mark_safe("<a href='{}'>{}</a>".format(url, lending.product.name))

class LendingInLine(admin.TabularInline, AdminURLMixin):
    model = Lending
    extra = 0
    readonly_fields = ["duree", "product_link"]
    fields = ["duree", "product_link"]
    verbose_name = "Réservation"
    verbose_name_plural = "Réservations"

    def has_add_permission(self, request):
        return False

    def product_link(self, lending):
        url = self.get_admin_url(lending.product)
        return mark_safe("<a href='{}'>{}</a>".format(url, lending.product.name))

    product_link.short_description = "Produit"


@admin.register(Student_recherche)
class Student_rechercheAdmin(admin.ModelAdmin):
    inlines = [LendingInLine,] #liste des réservations d'un étudiant

class NatureStudent_proposeInline(admin.TabularInline):
    model = Nature.students.through
    extra = 1
    verbose_name = "Produit"
    verbose_name_plural = "Produits"


@admin.register(Student_propose)
class Student_proposeAdmin(admin.ModelAdmin):
    inlines = [NatureStudent_proposeInline,]

@admin.register(Nature)
class NatureAdmin(admin.ModelAdmin):
    search_fields = ['name']