from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.db import IntegrityError, transaction
from jerecherche.models import Student_propose, Student_recherche, Nature, Lending
from jerecherche.forms import LendingForm

def index(request):
    """ permet d'accéder au template index"""
    return render_to_response("jerecherche/index.html")

def liste_jerecherche(request):
    """
    Affiche tous les produits proposés à l’échange
    """
    products = Nature.objects.all()
    context = {
        'products': products
    }
    return render(request, 'jerecherche/listing.html', context)

@transaction.atomic
def description_jerecherche(request, product_id):
    """Prend en argument les données du formulaire. Ajoute les coordonnées d'un étudiant recherchant un objet dans la base de données
    si celui-ci n'y est pas encore. Affiche un message de confirmation de réservation si le produit 
    demandé est disponible. Renvoie un message d'erreur sinon."""
    product = get_object_or_404(Nature, pk=product_id)
    students_name=[producer.name for producer in product.students.all()]
    context = {
        'product_name': product.name,
        'students_name': students_name,
        'product_id': product.id,
        'thumbnail': product.picture
    }

    if request.method == 'POST':
        form = LendingForm(request.POST)
        if form.is_valid():
            mail = form.cleaned_data['mail']
            name = form.cleaned_data['name']
 
            try:
                with transaction.atomic():
                    studentr = Student_recherche.objects.filter(mail=mail)
                    if not studentr.exists():
                    # Ajoute un nouvel étudiant à la table s'il n'y est pas encore
                        studentr = Student_recherche.objects.create(
                            mail=mail,
                            name=name
                        )
                    else:
                        studentr = studentr.first()

                    product = get_object_or_404(Nature, id=product_id)
                    lending = Lending.objects.create(
                        liaison=studentr,
                        product=product
                    )
                    product.available = False
                    product.save()
                    context = {
                        'product_name': product.name
                    }
                    return render(request, 'jerecherche/merci.html', context)
            except IntegrityError:
                form.errors['internal'] = "Oups ! Vous avez déjà réservé cet objet."
    
    else:
        # N'accepte pas les données au mauvais format
        # Ajoute les erreurs au template
        form = LendingForm()
    context['form'] = form
    context['errors'] = form.errors.items()
    return render(request, 'jerecherche/detail.html', context) #renvoie à une page de confirmation de réservation d'un objet


def search_jerecherche(request):
    """Affiche les étudiants qui proposent un objet si celui-ci est disponible"""
    query = request.GET.get('query')
    if not query:
        products = Nature.objects.all()
    else:
        products = Nature.objects.filter(name__icontains=query)

    if not products.exists():
        products = Nature.objects.filter(students__name__icontains=query)
    title = "Résultats pour la requête %s"%query
    context = {
        'products': products,
        'name': Student_propose.name
    }
    return render(request, 'jerecherche/search.html', context)
