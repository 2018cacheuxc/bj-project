# Generated by Django 2.1.3 on 2018-11-19 10:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Lending',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question_text', models.CharField(max_length=200)),
                ('date_lend', models.DateTimeField(verbose_name='lending date')),
            ],
        ),
        migrations.CreateModel(
            name='Object',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nature', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=40)),
                ('last_name', models.CharField(max_length=40)),
                ('phone', models.IntegerField(unique=True)),
                ('mail', models.EmailField(max_length=254)),
            ],
        ),
        migrations.AddField(
            model_name='object',
            name='student',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jerecherche.Student'),
        ),
        migrations.AddField(
            model_name='lending',
            name='object',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jerecherche.Object'),
        ),
        migrations.AddField(
            model_name='lending',
            name='student',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jerecherche.Student'),
        ),
    ]
