# Generated by Django 2.1.3 on 2018-11-19 15:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jerecherche', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lended_Object',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='Nature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
            ],
        ),
        migrations.RemoveField(
            model_name='lending',
            name='object',
        ),
        migrations.RemoveField(
            model_name='lending',
            name='student',
        ),
        migrations.RemoveField(
            model_name='object',
            name='student',
        ),
        migrations.DeleteModel(
            name='Lending',
        ),
        migrations.DeleteModel(
            name='Object',
        ),
        migrations.AddField(
            model_name='lended_object',
            name='nature',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jerecherche.Nature'),
        ),
        migrations.AddField(
            model_name='lended_object',
            name='student',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jerecherche.Student'),
        ),
    ]
