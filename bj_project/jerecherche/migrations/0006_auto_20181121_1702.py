# Generated by Django 2.1.3 on 2018-11-21 17:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jerecherche', '0005_nature_picture'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student_propose',
            name='phone',
        ),
        migrations.RemoveField(
            model_name='student_recherche',
            name='phone',
        ),
    ]
