
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import include, url 
from django.views.generic import ListView
import jepropose.views
from jerecherche.models import Student_propose, Student_recherche, Nature, Lending
import AboutUs.views


app_name='AboutUs'
urlpatterns = [    
    url(r'^$', AboutUs.views.page_aboutus, name='page_aboutus'),
    ]

