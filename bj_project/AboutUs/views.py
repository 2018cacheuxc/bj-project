from django.shortcuts import render
from django.http import HttpResponse

def page_aboutus(request):
    """Cette fonction permet l'affichage de la page About Us qui raconte qui nous sommes: Elle crée un lien vers le fichier html listing"""
    context={}    
    return render(request, 'AboutUs/listing.html', context)
