from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import include, url 
from django.views.generic import ListView
import jepropose.views
from jerecherche.models import Student_propose, Student_recherche, Nature, Lending

app_name='jepropose'
urlpatterns = [    
    url(r'^$', jepropose.views.page_jepropose, name='page_jepropose'),
    ]

