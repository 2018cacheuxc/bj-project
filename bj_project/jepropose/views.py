from django.shortcuts import render
from django.http import HttpResponse
from jerecherche.models import Student_propose, Student_recherche, Nature, Lending
from jepropose.forms import ProposingForm
from django import forms
from django.views.generic import FormView


def page_jepropose(request):
    """Cette fonction permet d'afficher la page je propose qui permet à tous les étudiants de proposer sur le site les objets qu'ils souhaitent prêter.
    Dans un premier temps, la fonction recueille les données que l'utilisateur rentre (mail,nom,nature)
    puis si l'utilisateur n'est pas encore rentré dans la base de donnée, la fonction l'ajoute.
    De même si l'objet n'existe pas encore(n'a jamais été proposé) on l'ajoute à la table.
    Enfin on ouvre la page qui remercie un utilisateur qui a proposé un objet. """
    context={}    
    if request.method == 'POST':
        form = ProposingForm(request.POST)
        if form.is_valid():
            mail = form.cleaned_data['mail']
            name = form.cleaned_data['name']
            nature=form.cleaned_data['nature']

            studentp = Student_propose.objects.filter(mail=mail)
            if not studentp.exists():
                # Si l'étudiant n'est pas dans la base de données on l'ajoute à celle-ci
                studentp = Student_propose.objects.create(
                    mail=mail,
                    name=name,
                )
            else:
                studentp = studentp.first()

            newobject = Nature.objects.filter(name=nature)
            if not newobject.exists():
                # Si l'objet n'existe pas on l'ajoute à la table
                newobject = Nature.objects.create(name=nature, students=studentp)

            else:
                newobject = newobject.first()

            context = {
                'product_name': newobject.name
            }
            return render(request, 'jepropose/merci.html', context)
        else:
            # Si le format n'est pas celui attendu
            # Renvoie une erreur dans ce cas
            context['errors'] = form.errors.items()
    else:
        form = ProposingForm()
    context['form'] = form
    return render(request, 'jepropose/listing.html', context)
